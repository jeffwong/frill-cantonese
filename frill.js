var Frill = {
    active: false,
    activeButYetToInit: false,
    cursorX: 0,
    cursorY: 0,
    element: null,
    popup: null,
}

function decorate(span) {
    // console.log("decorating %o", span);
    if ($(span).children("frilly.frill-character").length > 0) {
        // Already decorated.
        return;
    }
    var text = $(span).text();
    if (text == undefined) {
        return;
    }
    // console.log("text: %s", text);
    var newHtml = "";
    for (var i=0; i < text.length; i++) {
        var character = text.charAt(i);
        if (isWhitespace(character)) {
            // Ignore whitespace.
            newHtml += character;
        } else {
            newHtml += '<frilly class="frill-character">' + character + '</frilly>';
        }
    }
    // console.log(newHtml);
    $(span).html(newHtml);
}

function undecorate(element) {
    // console.log("undecorating");
    $("frilly.frill frilly.frill-character", element).replaceWith(function() {
        return $(this).contents();
    });
}

function prime(element) {
    // Never prime certain elements.
    var blacklist = ['textarea'];
    if (blacklist.indexOf($(element).prop('tagName').toLowerCase()) > -1) {
        return;
    }
    if ($(element).hasClass('frill') || $(element).hasClass('frill-character')) {
        // Internal element, ignore.
        return;
    }
    if ($(element).attr('frilled')) {
        // Already primed, nothing to do.
        // console.log("element: %o already primed", element);
        return;
    }
    if ($(element).find('[frilled=true]').length > 0) {
        // A child element is already primed, nothing to do.
        // console.log("element: %o has a primed child element.", element);
        return;
    }
    if (!hasHanzi($(element).text())) {
        // No point priming an element with no hanzi.
        return;
    }
    $(element).parents("[frilled=true]").each(function() {
        // We only want one element primed at a time.
        // console.log("Unpriming parent: %o", this);
        unprime(this);
    });
    // console.log("priming element: %o", element);
    $(element).contents().filter(function() {
        return this.nodeType === Node.TEXT_NODE && !isWhitespace(this.data);
    }).wrap('<frilly class="frill"></frilly>');
    $("frilly.frill", element).each(function() {
        decorate(this);
    });
    $(element).attr('frilled', 'true');
    $(element).on('mouseleave.frill', function(event) {
        unprime(element);
    });
}

function unprime(element) {
    // console.log("unprime element: %o", element);
    $(element).off('mouseleave.frill');
    undecorate(element);
    $(element).children('frilly.frill').contents().unwrap();
    $(element).removeAttr('frilled');
    $(element).get(0).normalize();
}

function unmarkAll(element) {
    var parent_el = $(element).parents().last();
    var marked_parent_el = $("mark.frill", parent_el).parent().get(0);
    if (typeof marked_parent_el !== 'undefined') {
        $("mark.frill", marked_parent_el).contents().unwrap();
        marked_parent_el.normalize();
    }
}

function mark(element, message) {
    // Ensure we highlight all the words in whatever defintion we're displaying.
    unmarkAll(element);
    var element_list = $(element);
    var current_element = element;
    for (var i=1; i<message.length; i++) {
        current_element = $(current_element).next('frilly.frill-character');
        element_list = $(element_list).add(current_element);
    }
    $(element_list).wrapAll('<mark class="frill"></mark>');
}

function refresh(event) {
    var element = event.target;
    // No need to do anything if the element hasn't changed.
    if (element === Frill.element) {
        return;
    }
    Frill.cursorX = Frill.cursorY = 0;
    Frill.element = null;
    if ($(element).is("html") || $(element).is("body")) {
        unmarkAll(element);
        $(Frill.popup).hide();
        return;
    }
    if (!$(element).hasClass("frill-character")) {
        unmarkAll(element);
        $(Frill.popup).hide();
        prime(element);
        return;
    }
    var greedy_text = greedyMatch(element);
    if (!hasHanzi(greedy_text)) {
        // If there's no Hanzi in there, there's no point looking up anything.
        return;
    }
    Frill.element = element;
    Frill.cursorX = event.clientX;
    Frill.cursorY = event.clientY;
    safari.self.tab.dispatchMessage("lookupWord", greedy_text);
}

function toast(str) {
    // Only have the toast messages show in the non-embedded window.
    if (isIframe()) return false;
    var duration = 3000;
    var toaster = $('<div id="frill-toast"></div>').css({
        'z-index': '999999',
        display: 'none',
        position: 'absolute',
        top: $(window).scrollTop(),
        left: $(window).scrollLeft(),
        padding: '1em',
        border: '1px solid #ccc',
        'background-color': 'white',
        color: '#111',
    }).text(str).appendTo("body").fadeIn();
    window.setTimeout(function() {
        toaster.fadeOut({
            done: function () { toaster.detach(); }
        });
    }, duration);
}

function deactivate() {
    if (Frill.active === false) {
        return;
    }
    console.log("Deactivating Frill.");
    unmarkAll($("body"));
    $('[frilled=true]').each(function(event) {
        unprime(this);
    });
    $(document).off('mousemove.frill');
    Frill.popup.detach();
    Frill.active = false;
    toast("Frill has been turned off.");
}

function activate() {
    if (Frill.active === true) {
        return;
    }
    console.log("Activating Frill.");
    Frill.popup.appendTo("body");
    $(document).on('mousemove.frill', function(event) {
        refresh(event);
    });
    if (Frill.activeButYetToInit === true) {
        Frill.activeButYetToInit = false;
    } else {
        toast("Frill has been turned on.");
    }
    Frill.active = true;
}

function respondToMessage(event) {
    // console.log("Receiving message: %o", event);
    // Global messages intended for all windows/tabs/pages.
    if (event.name === "status") {
        if (event.message == "deactivate") {
            deactivate();
            return;
        } else if (event.message === "activate") {
            activate();
            return;
        }
    }
    // All other messages must be targeted.
    if (event.message.target_url != window.location.href) {
        // Must be meant for another frame.
        return;
    }
    if (event.name === "wordDetails") {
        if (Frill.cursorX == 0 && Frill.cursorY == 0) {
            // This is a hack to avoid the pop-up showing in frames other than the one where the content is.
            return;
        }
        showPopup(event.message.entries)
        if (event.message.entries.length > 0) {
            mark(Frill.element, event.message.entries[0].simplified)
        }
        return;
    }
    if (event.name === "currentlyActive") {
        // console.log("Turns out the active status is: %o", event.message.active);
        if (event.message.active === true) {
            Frill.activeButYetToInit = true;
            activate();
        }
        return;
    }
}

function showPopup(entries) {
    $(Frill.popup).css({ top: Frill.cursorY+$(window).scrollTop()+10, left: Frill.cursorX+$(window).scrollLeft()+10 })
    var contents = ''
    for (var i=0; i<entries.length; i++) {
        var details = entries[i]
        var header = "<h3>" + details.simplified
        if (details.traditional) {
            header += " (" + details.traditional + ")"
        }
        if (details.pronunciation) {
            header += " <em>" + formatPronunciation(details.pronunciation) + "</em>"
        }
        header += "</h3>"
        contents += header + "<ul>" + $.map(details.definitions, function(val, i) { return "<li>" + val + "</li>"; }).join("\n") + "</ul>"
    }
    $(Frill.popup).html(contents).show()
}

function greedyMatch(element) {
    // Looks at sibling frill elements to find the longest theoretical dictionary match.
    // We naively grab everything we can up to a stop word (or we hit 60 characters, the max in our dictionary.)
    var still_hungry = true;
    var text = '';
    var current_element = element;
    var count = 0;
    while (still_hungry) {
        if (current_element.length === 0 || count > 60) {
            break;
        }
        var content = $(current_element).text();
        if (/^[A-Z0-9 ]$/i.test(content) || (isHanzi(content) && !/[，。、：；·“”（）「」]/.test(content))) {
            text += content;
        } else {
            still_hungry = false;
        }
        current_element = $(current_element).next('frilly.frill-character, mark.frill');
        count++;
    }
    // console.log("greedyMatch: %s", text);
    return text;
}

var styling = ''
styling += "div#frill { z-index: 999999; position: absolute; max-width: 333px; padding: 0.5em 1em; padding-bottom: 1em; }\n"
styling += "div#frill { border: 1px solid #ccc; background-color: #fff; color: #111; }\n"
styling += "div#frill { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 10pt; line-height: 80% }\n"
styling += "div#frill h3 { font-size: 12pt; line-height: normal; font-weight: bolder; font-size: 1.17em; margin: .83em 0; padding: 0; }\n"
styling += "div#frill h3 em { font-style: italic; font-weight: bolder; }\n"
styling += "div#frill ul { display: block; list-style-type: disc; list-style-image: none; line-height: normal; margin: 0; padding: 0; padding-left: 2em; }\n"
styling += "div#frill ul li { display: list-item; }\n"
$("head").append("<style> " + styling + " </style>");

Frill.popup = $('<div id="frill"></div>').hide();

$(document).ready(function() {
    if (typeof safari !== 'undefined') {
        safari.self.addEventListener("message", respondToMessage, false);
        // console.log("Checking to see if I should start activated or not.");
        safari.self.tab.dispatchMessage("isCurrentlyActive");
    }
});
